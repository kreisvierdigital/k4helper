<?php
/**
 * k4helper plugin for Craft CMS 3.x
 *
 * k4helper
 *
 * @link      www.kreisvier.ch
 * @copyright Copyright (c) 2018 k4
 */

/**
 * k4helper en Translation
 *
 * Returns an array with the string to be translated (as passed to `Craft::t('k4helper', '...')`) as
 * the key, and the translation as the value.
 *
 * http://www.yiiframework.com/doc-2.0/guide-tutorial-i18n.html
 *
 * @author    k4
 * @package   K4helper
 * @since     1.0.0
 */
return [
    'k4helper plugin loaded' => 'k4helper plugin loaded',
];
