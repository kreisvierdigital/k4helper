# k4helper plugin for Craft CMS 3.x

k4helper

![Screenshot](resources/img/plugin-logo.png)

## Requirements

This plugin requires Craft CMS 3.0.0-RC1 or later.

## Installation

To install the plugin, follow these instructions.

1. Open your terminal and go to your Craft project:

        cd /path/to/project

2. Then tell Composer to load the plugin:

        composer require k4/k4helper

3. In the Control Panel, go to Settings → Plugins and click the “Install” button for k4helper.

## k4helper Overview

A handy plugin to purge litespeed cache

## Configuring k4helper

-Insert text here-

## Using k4helper

-Insert text here-

## k4helper Roadmap

Some things to do, and ideas for potential features:

* Release it

Brought to you by [k4](www.kreisvier.ch)
